import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Frontend3Component } from './frontend3.component';

describe('Frontend3Component', () => {
  let component: Frontend3Component;
  let fixture: ComponentFixture<Frontend3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Frontend3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Frontend3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
